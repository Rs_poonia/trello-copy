const listWrapper = document.getElementById('myDIV');
let addButton = listWrapper.addEventListener('click', event => {
  let name = event.target.name;
  document.getElementById('exampleModalLongTitle').innerHTML = name;

  // add new card using add card button
  if (event.path[0].className === 'addBtn' && document.getElementById("myInput").value !== '') {
    createCard(document.getElementById("myInput").value).then(res => {
      newElement(res.name, res.id, "myUL");
    })
  }
  else if(event.target.classList.value==='card-list'){
    getAllCheckList(event.target.getAttribute('data-id'));
  }
  // here we are delete a card whichever we want to delete
  else if (event.target.innerText === '×' && event.path[1].className==='card-list' ) {
    deleteCard(event.target.parentNode.getAttribute('data-id'));
  }
  else if (event.target.dataset.action === 'add-checklist') {
     postChecklist(event.path[4].id, document.getElementById('checklist-input').value).then(res=>{
      createChecklistForEachCard(res.name,res.id);
    })  
  }
 else if(event.target.innerText==='-'){
  //  console.log(event.target.parentNode.parentNode.parentNode.getAttribute('dataid'))
  deleteChecklistItem(event.target.parentNode.getAttribute('dataid'),event.target.parentNode.parentNode.parentNode.getAttribute('dataid'),event);
 }
 else if(event.path[0].className==='close-checklist'){
    removeCompleteCheckListTrello(event.path[1].getAttribute('dataid')).then(res=>{
      event.path[1].remove();
    })   
  }
  else if(event.path[0].className==='checklist-create'){
    createChecklistItemInTrello(document.getElementById("checklist-item-input").value, event.path[0].parentNode.children[3].id)
    createChecklistItem(document.getElementById("checklist-item-input").value, event.path[0].parentNode.children[3].id);
    // console.log(event.path[0].parentNode.children[3].id)
  }
  
  getAllCheckListItem(event.target.getAttribute('dataid'));
  // else {
  const modal = event.target.parentNode.parentNode.parentNode.lastElementChild;
  // modal.children[0].children[0].children[1].firstElementChild.innerHTML = '';

  modal.setAttribute('id', event.target.getAttribute('data-id'))

});


const yourApiKey = '567a0add2e3df8fa382a92e97f361ed7';
const yourApiToken = '6932831b3e8708e1f8560208068daea4a7111e023ed62a02e7cf0d5a4fb8e79a';
const boardId = '5e05a1e7481df236d5412654';
const firstListId = '5e05f2482f852c2f7bf49dfe';

// for create a new card using api
async function createCard(inputValue) {
  const result = await fetch(`https://api.trello.com/1/cards?idList=${firstListId}&keepFromSource=all&key=${yourApiKey}&token=${yourApiToken}&name=${inputValue}`, {
    method: 'post'
  });
  return await result.json();
}

// delete a card using api call
async function deleteCard(cardId) {
  if(cardId!==undefined){
    await fetch(`https://api.trello.com/1/cards/${cardId}?key=${yourApiKey}&token=${yourApiToken}`, {
      method: 'delete'
    });
    let el = document.querySelector(`[data-id="${cardId}"]`);
    // console.log(el)
    el.remove();
  }
 
}

// get all cards on load window on screen
async function getAllCards() {
  const res = await fetch(`https://api.trello.com/1/lists/${firstListId}/cards?key=${yourApiKey}&token=${yourApiToken}`)
  const data = await res.json();
  
  data.forEach(element => {
    newElement(element.name, element.id, "myUL")
  });
}
getAllCards();

// open pop up window for particular id and we pass the name of window
async function getAllCheckList(cardDataId) {
  const checklists = await fetch(`https://api.trello.com/1/cards/${cardDataId}/checklists?checkItems=all&key=${yourApiKey}&token=${yourApiToken}&checkItem_fields=name%2Cstate`)
  let data = await checklists.json();

  data.forEach(element => {
    if(element.name!==''){
      createChecklistForEachCard(element.name, element.id)
    }
  })
}


async function postChecklist(cardId,checkListName) {
  let newCheckListItem = await fetch(`https://api.trello.com/1/checklists?idCard=${cardId}&name=${checkListName}&key=${yourApiKey}&token=${yourApiToken}`,
  {
    method: 'POST'
  })
  return newCheckListItem.json();
}
async function removeCompleteCheckListTrello(checkListID) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/checklists/${checkListID}?key=${yourApiKey}&token=${yourApiToken}`,
      {
        method: 'delete'
      }
    );
  } catch (error) {
    console.log(`error ${error}`);
  }
}

async function createChecklistItemInTrello(name,checklistid){
  
 let res = await fetch(`https://api.trello.com/1/checklists/${checklistid}/checkItems?name=${name}&pos=bottom&checked=false&key=${yourApiKey}&token=${yourApiToken}`,{
  method: 'POST'
}).then(res=>{
  createChecklistForEachCard(res.name,res.id)
})
  let data = await res.json();
  createChecklistItem(data.name,data.id);
}
async function getAllCheckListItem(checklistid){
 let res = await fetch(`https://api.trello.com/1/checklists/${checklistid}?fields=name&cards=all&card_fields=name&key=${yourApiKey}&token=${yourApiToken}`)
  let data = await res.json();
  data.checkItems.forEach(item=>{
    // console.log(item.id,item.name,item.idChecklist)
    createChecklistItem(item.name,item.idChecklist,item.id);
  })
  // console.log(data.checkItems)
}
async function deleteChecklistItem(checkitemId,checklistId,event){
    await fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${yourApiKey}&token=${yourApiToken}`,{
      method:'DELETE'
    })
  event.path[1].remove();
  // document.getElementById(checkitemId).remove();
}
function createChecklistForEachCard(inputValue, checklistid) {
  let li = document.createElement("li");
  let t = document.createTextNode(inputValue);
  li.className='checklist';
  li.setAttribute('dataId',checklistid);
  li.appendChild(t);
  if (inputValue === '') {
    alert("for add card write a card");
  } else {
    document.getElementById('modal-body').appendChild(li);
  }  
  document.getElementById("checklist-input").value = "";
  let span = document.createElement("SPAN");
  span.className = "checklist-create";
  let txt = document.createTextNode("\u002B");
  span.appendChild(txt);
  li.appendChild(span);
  let div = document.createElement("DIV");
  txt = document.createTextNode("\u00D7");
  div.className = "close-checklist";
  div.appendChild(txt);
  li.appendChild(div);
  let inputItem = document.createElement('input');
  inputItem.type='text';
  inputItem.id='checklist-item-input';
  inputItem.placeholder='checklist item...';
  li.appendChild(inputItem)
  let ul = document.createElement('UL');
  ul.className = 'checklist-item-list';
  ul.id = checklistid;
  li.appendChild(ul);
}

function newElement(inputValue, cardId, listid) {

  // console.log(inputValue,cardId)
  let li = document.createElement("li");
  // li.setAttribute('data-target',)
  li.name = inputValue;
  let t = document.createTextNode(inputValue);

  // model addedd for open popup window
  li.setAttribute('data-toggle', 'modal');
  li.className = ('card-list');
  li.setAttribute('data-target', '#' + cardId);
  li.setAttribute('data-id', cardId)
  li.appendChild(t);
  if (inputValue === '') {
    alert("for add card write a card");
  } else {
    document.getElementById(listid).appendChild(li);
  }
  document.getElementById("myInput").value = "";
  let span = document.createElement("SPAN");
  let txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);
}


function createChecklistItem(inputValue,checklistid,itemid){
  let li = document.createElement("li");
  let t = document.createTextNode(inputValue);  
  li.setAttribute('dataid',itemid)
  li.className='checklist-item';
  li.appendChild(t);
  document.getElementById(checklistid).appendChild(li);  
  let checkbox = document.createElement("input");
  checkbox.type = 'checkbox';
  checkbox.id='';
  checkbox.className = 'checkbox';
  checkbox.name = 'checkbox';
  li.appendChild(checkbox)
  let div = document.createElement("div");
  let deletechekitem = document.createTextNode("-");
  deletechekitem.className = 'delete-item';
  div.appendChild(deletechekitem);
  li.appendChild(div)
}